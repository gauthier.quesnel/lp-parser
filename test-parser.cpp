/* Copyright (C) 2018-2023 INRAE
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "lp-parser.hpp"

#include <cassert>
#include <fstream>
#include <sstream>

#ifndef EXAMPLES_DIR
#define EXAMPLES_DIR "examples"
#endif

constexpr std::size_t operator"" _ul(unsigned long long int v)
{
    return static_cast<std::size_t>(v);
}

constexpr baryonyx::index operator"" _i(unsigned long long int v)
{
    return static_cast<baryonyx::index>(v);
}

void
named_objective()
{
    const char* example = "maximize\n"
                          "x0: +x1 + 2x2 + 3x3 - 100\n"
                          "end\n";

    std::istringstream iss(example);

    auto pb = baryonyx::make_problem(iss);
    assert(pb);
    assert(pb.type == baryonyx::objective_function_type::maximize);
    assert(pb.objective.elements.size() == 3_ul);
    assert(pb.objective.elements[0].factor == 1_i);
    assert(pb.objective.elements[0].variable_index == 0_i);
    assert(pb.objective.elements[1].factor == 2_i);
    assert(pb.objective.elements[1].variable_index == 1_i);
    assert(pb.objective.elements[2].factor == 3_i);
    assert(pb.objective.elements[2].variable_index == 2_i);
    assert(pb.objective.value == -100.0);
}

void
no_named_objective()
{
    const char* example = "maximize\n"
                          "st: x1 + x2 + x3 = 1\n"
                          "end\n";

    std::istringstream iss(example);

    auto pb = baryonyx::make_problem(iss);
    assert(pb);
    assert(pb.type == baryonyx::objective_function_type::maximize);
    assert(pb.objective.elements.size() == 0_ul);
    assert(pb.vars.names.size() == 3_ul);
    assert(pb.vars.values.size() == 3_ul);
    assert(pb.less_constraints.size() == 0_ul);
    assert(pb.greater_constraints.size() == 0_ul);
    assert(pb.equal_constraints.size() == 1_ul);
}

void
small_lp()
{
    const char* example_1 = "maximize\n"
                            "obj: x1 + 2x2 + 3x3 - 100\n"
                            "st\n"
                            "time:  -x1 + x2 + x3 <= 20\n"
                            "labor:  x1 - 3x2 + x3 <= 30\n"
                            "test: x1 - 3x2 + x3 <= -5\n"
                            "bounds\n"
                            "x1 <= 40\n"
                            "end\n";

    std::istringstream iss(example_1);

    auto pb = baryonyx::make_problem(iss);
    assert(pb);
    assert(pb.type == baryonyx::objective_function_type::maximize);
    assert(pb.objective.elements.size() == 3_ul);
    assert(pb.objective.elements[0].factor == 1_i);
    assert(pb.objective.elements[0].variable_index == 0_i);
    assert(pb.objective.elements[1].factor == 2_i);
    assert(pb.objective.elements[1].variable_index == 1_i);
    assert(pb.objective.elements[2].factor == 3_i);
    assert(pb.objective.elements[2].variable_index == 2_i);
    assert(pb.objective.value == -100.0);

    assert(pb.vars.names.size() == 3_ul);
    assert(pb.vars.values.size() == 3_ul);
    assert(pb.less_constraints.size() == 3_ul);

    assert(pb.less_constraints[0].elements.size() == 3_ul);
    assert(pb.less_constraints[0].elements[0].factor == -1);
    assert(pb.less_constraints[0].elements[0].variable_index == 0_i);
    assert(pb.less_constraints[0].elements[1].factor == 1_i);
    assert(pb.less_constraints[0].elements[1].variable_index == 1_i);
    assert(pb.less_constraints[0].elements[2].factor == 1_i);
    assert(pb.less_constraints[0].elements[2].variable_index == 2_i);
    assert(pb.less_constraints[0].value == 20);

    assert(pb.less_constraints[1].elements.size() == 3_ul);
    assert(pb.less_constraints[1].elements[0].factor == 1_i);
    assert(pb.less_constraints[1].elements[0].variable_index == 0_i);
    assert(pb.less_constraints[1].elements[1].factor == -3_i);
    assert(pb.less_constraints[1].elements[1].variable_index == 1_i);
    assert(pb.less_constraints[1].elements[2].factor == 1_i);
    assert(pb.less_constraints[1].elements[2].variable_index == 2_i);
    assert(pb.less_constraints[1].value == 30);

    assert(pb.less_constraints[2].elements.size() == 3_ul);
    assert(pb.less_constraints[1].elements[0].factor == 1_i);
    assert(pb.less_constraints[2].elements[0].variable_index == 0_i);
    assert(pb.less_constraints[2].elements[1].factor == -3_i);
    assert(pb.less_constraints[2].elements[1].variable_index == 1_i);
    assert(pb.less_constraints[2].elements[2].factor == 1_i);
    assert(pb.less_constraints[2].elements[2].variable_index == 2_i);
    assert(pb.less_constraints[2].value == -5);

    assert(pb.vars.names[0] == "x1");
    assert(pb.vars.names[1] == "x2");
    assert(pb.vars.names[2] == "x3");

    assert(pb.vars.values[0].min == 0_i);
    assert(pb.vars.values[1].min == 0_i);
    assert(pb.vars.values[2].min == 0_i);

    assert(pb.vars.values[0].max == 40_i);
    assert(pb.vars.values[1].max == std::numeric_limits<int>::max());
    assert(pb.vars.values[2].max == std::numeric_limits<int>::max());
}

void
quadratic_lp()
{
    const char* example_1 = "maximize\n"
                            "obj: x1 + 2x2 + 3x3 "
                            "- [] /2 - 100\n"
                            "st\n"
                            "time:  -x1 + x2 + x3 <= 20\n"
                            "labor:  x1 - 3x2 + x3 <= 30\n"
                            "test: x1 - 3x2 + x3 <= -5\n"
                            "bounds\n"
                            "x1 <= 40\n"
                            "end\n";

    std::istringstream iss(example_1);

    auto pb = baryonyx::make_problem(iss);

    assert(pb);
    assert(pb.type == baryonyx::objective_function_type::maximize);
    assert(pb.objective.elements.size() == 3);
    assert(pb.objective.elements[0].factor == 1);
    assert(pb.objective.elements[0].variable_index == 0);
    assert(pb.objective.elements[1].factor == 2);
    assert(pb.objective.elements[1].variable_index == 1);
    assert(pb.objective.elements[2].factor == 3);
    assert(pb.objective.elements[2].variable_index == 2);

    assert(pb.objective.qelements.size() == 0);

    assert(pb.objective.value == -100);

    assert(pb.vars.names.size() == 3);
    assert(pb.vars.values.size() == 3);
    assert(pb.less_constraints.size() == 3);

    assert(pb.less_constraints[0].elements.size() == 3);
    assert(pb.less_constraints[0].elements[0].factor == -1);
    assert(pb.less_constraints[0].elements[0].variable_index == 0);
    assert(pb.less_constraints[0].elements[1].factor == 1);
    assert(pb.less_constraints[0].elements[1].variable_index == 1);
    assert(pb.less_constraints[0].elements[2].factor == 1);
    assert(pb.less_constraints[0].elements[2].variable_index == 2);
    assert(pb.less_constraints[0].value == 20);

    assert(pb.less_constraints[1].elements.size() == 3);
    assert(pb.less_constraints[1].elements[0].factor == 1);
    assert(pb.less_constraints[1].elements[0].variable_index == 0);
    assert(pb.less_constraints[1].elements[1].factor == -3);
    assert(pb.less_constraints[1].elements[1].variable_index == 1);
    assert(pb.less_constraints[1].elements[2].factor == 1);
    assert(pb.less_constraints[1].elements[2].variable_index == 2);
    assert(pb.less_constraints[1].value == 30);

    assert(pb.less_constraints[2].elements.size() == 3);
    assert(pb.less_constraints[1].elements[0].factor == 1);
    assert(pb.less_constraints[2].elements[0].variable_index == 0);
    assert(pb.less_constraints[2].elements[1].factor == -3);
    assert(pb.less_constraints[2].elements[1].variable_index == 1);
    assert(pb.less_constraints[2].elements[2].factor == 1);
    assert(pb.less_constraints[2].elements[2].variable_index == 2);
    assert(pb.less_constraints[2].value == -5);

    assert(pb.vars.names[0] == "x1");
    assert(pb.vars.names[1] == "x2");
    assert(pb.vars.names[2] == "x3");

    assert(pb.vars.values[0].min == 0);
    assert(pb.vars.values[1].min == 0);
    assert(pb.vars.values[2].min == 0);

    assert(pb.vars.values[0].max == 40);
    assert(pb.vars.values[1].max == std::numeric_limits<int>::max());
    assert(pb.vars.values[2].max == std::numeric_limits<int>::max());
}

void
quadratic_2()
{
    const char* example_1 = "maximize\n"
                            "obj: x1 + 2x2 + 3x3 "
                            "- [ -x1^2 - 17x1*x2 + 5x2^2] /2 - 100\n"
                            "st\n"
                            "time:  -x1 + x2 + x3 <= 20\n"
                            "labor:  x1 - 3x2 + x3 <= 30\n"
                            "test: x1 - 3x2 + x3 <= -5\n"
                            "bounds\n"
                            "x1 <= 40\n"
                            "end\n";

    std::istringstream iss(example_1);

    auto pb = baryonyx::make_problem(iss);

    assert(pb);
    assert(pb.type == baryonyx::objective_function_type::maximize);
    assert(pb.objective.elements.size() == 3);
    assert(pb.objective.elements[0].factor == 1);
    assert(pb.objective.elements[0].variable_index == 0);
    assert(pb.objective.elements[1].factor == 2);
    assert(pb.objective.elements[1].variable_index == 1);
    assert(pb.objective.elements[2].factor == 3);
    assert(pb.objective.elements[2].variable_index == 2);

    assert(pb.objective.qelements.size() == 3);

    assert(pb.objective.qelements[0].factor == 1.0 / 2.0);
    assert(pb.objective.qelements[0].variable_index_a == 0);
    assert(pb.objective.qelements[0].variable_index_b == 0);
    assert(pb.objective.qelements[1].factor == 17.0 / 2.0);
    assert(pb.objective.qelements[1].variable_index_a == 0);
    assert(pb.objective.qelements[1].variable_index_b == 1);
    assert(pb.objective.qelements[2].factor == -5.0 / 2.0);
    assert(pb.objective.qelements[2].variable_index_a == 1);
    assert(pb.objective.qelements[2].variable_index_b == 1);

    assert(pb.objective.value == -100);

    assert(pb.vars.names.size() == 3);
    assert(pb.vars.values.size() == 3);
    assert(pb.less_constraints.size() == 3);

    assert(pb.less_constraints[0].elements.size() == 3);
    assert(pb.less_constraints[0].elements[0].factor == -1);
    assert(pb.less_constraints[0].elements[0].variable_index == 0);
    assert(pb.less_constraints[0].elements[1].factor == 1);
    assert(pb.less_constraints[0].elements[1].variable_index == 1);
    assert(pb.less_constraints[0].elements[2].factor == 1);
    assert(pb.less_constraints[0].elements[2].variable_index == 2);
    assert(pb.less_constraints[0].value == 20);

    assert(pb.less_constraints[1].elements.size() == 3);
    assert(pb.less_constraints[1].elements[0].factor == 1);
    assert(pb.less_constraints[1].elements[0].variable_index == 0);
    assert(pb.less_constraints[1].elements[1].factor == -3);
    assert(pb.less_constraints[1].elements[1].variable_index == 1);
    assert(pb.less_constraints[1].elements[2].factor == 1);
    assert(pb.less_constraints[1].elements[2].variable_index == 2);
    assert(pb.less_constraints[1].value == 30);

    assert(pb.less_constraints[2].elements.size() == 3);
    assert(pb.less_constraints[1].elements[0].factor == 1);
    assert(pb.less_constraints[2].elements[0].variable_index == 0);
    assert(pb.less_constraints[2].elements[1].factor == -3);
    assert(pb.less_constraints[2].elements[1].variable_index == 1);
    assert(pb.less_constraints[2].elements[2].factor == 1);
    assert(pb.less_constraints[2].elements[2].variable_index == 2);
    assert(pb.less_constraints[2].value == -5);

    assert(pb.vars.names[0] == "x1");
    assert(pb.vars.names[1] == "x2");
    assert(pb.vars.names[2] == "x3");

    assert(pb.vars.values[0].min == 0);
    assert(pb.vars.values[1].min == 0);
    assert(pb.vars.values[2].min == 0);

    assert(pb.vars.values[0].max == 40);
    assert(pb.vars.values[1].max == std::numeric_limits<int>::max());
    assert(pb.vars.values[2].max == std::numeric_limits<int>::max());
}

void
quadratic_3()
{
    const char* example_1 = "maximize\n"
                            "obj: x1 + 2x2 + 3x3 "
                            "- [ -x1*x2 - 17x1*x2 + 5x1*x2] /2 - 100\n"
                            "st\n"
                            "time:  -x1 + x2 + x3 <= 20\n"
                            "labor:  x1 - 3x2 + x3 <= 30\n"
                            "test: x1 - 3x2 + x3 <= -5\n"
                            "bounds\n"
                            "x1 <= 40\n"
                            "end\n";

    std::istringstream iss(example_1);

    auto pb = baryonyx::make_problem(iss);

    assert(pb);
    assert(pb.type == baryonyx::objective_function_type::maximize);
    assert(pb.objective.elements.size() == 3);
    assert(pb.objective.elements[0].factor == 1);
    assert(pb.objective.elements[0].variable_index == 0);
    assert(pb.objective.elements[1].factor == 2);
    assert(pb.objective.elements[1].variable_index == 1);
    assert(pb.objective.elements[2].factor == 3);
    assert(pb.objective.elements[2].variable_index == 2);

    assert(pb.objective.qelements.size() == 1);

    assert(pb.objective.qelements[0].factor == 13.0 / 2.0);
    assert(pb.objective.qelements[0].variable_index_a == 0);
    assert(pb.objective.qelements[0].variable_index_b == 1);

    assert(pb.objective.value == -100);

    assert(pb.vars.names.size() == 3);
    assert(pb.vars.values.size() == 3);
    assert(pb.less_constraints.size() == 3);

    assert(pb.less_constraints[0].elements.size() == 3);
    assert(pb.less_constraints[0].elements[0].factor == -1);
    assert(pb.less_constraints[0].elements[0].variable_index == 0);
    assert(pb.less_constraints[0].elements[1].factor == 1);
    assert(pb.less_constraints[0].elements[1].variable_index == 1);
    assert(pb.less_constraints[0].elements[2].factor == 1);
    assert(pb.less_constraints[0].elements[2].variable_index == 2);
    assert(pb.less_constraints[0].value == 20);

    assert(pb.less_constraints[1].elements.size() == 3);
    assert(pb.less_constraints[1].elements[0].factor == 1);
    assert(pb.less_constraints[1].elements[0].variable_index == 0);
    assert(pb.less_constraints[1].elements[1].factor == -3);
    assert(pb.less_constraints[1].elements[1].variable_index == 1);
    assert(pb.less_constraints[1].elements[2].factor == 1);
    assert(pb.less_constraints[1].elements[2].variable_index == 2);
    assert(pb.less_constraints[1].value == 30);

    assert(pb.less_constraints[2].elements.size() == 3);
    assert(pb.less_constraints[1].elements[0].factor == 1);
    assert(pb.less_constraints[2].elements[0].variable_index == 0);
    assert(pb.less_constraints[2].elements[1].factor == -3);
    assert(pb.less_constraints[2].elements[1].variable_index == 1);
    assert(pb.less_constraints[2].elements[2].factor == 1);
    assert(pb.less_constraints[2].elements[2].variable_index == 2);
    assert(pb.less_constraints[2].value == -5);

    assert(pb.vars.names[0] == "x1");
    assert(pb.vars.names[1] == "x2");
    assert(pb.vars.names[2] == "x3");

    assert(pb.vars.values[0].min == 0);
    assert(pb.vars.values[1].min == 0);
    assert(pb.vars.values[2].min == 0);

    assert(pb.vars.values[0].max == 40);
    assert(pb.vars.values[1].max == std::numeric_limits<int>::max());
    assert(pb.vars.values[2].max == std::numeric_limits<int>::max());
}

void
quadratic_4()
{
    const char* example_1 = "maximize\n"
                            "obj: x1 + 2x2 + 3x3 "
                            "+ [] /2 - 100\n"
                            "st\n"
                            "time:  -x1 + x2 + x3 <= 20\n"
                            "labor:  x1 - 3x2 + x3 <= 30\n"
                            "test: x1 - 3x2 + x3 <= -5\n"
                            "bounds\n"
                            "x1 <= 40\n"
                            "end\n";

    std::istringstream iss(example_1);

    auto pb = baryonyx::make_problem(iss);

    assert(pb);
    assert(pb.type == baryonyx::objective_function_type::maximize);
    assert(pb.objective.elements.size() == 3);
    assert(pb.objective.elements[0].factor == 1);
    assert(pb.objective.elements[0].variable_index == 0);
    assert(pb.objective.elements[1].factor == 2);
    assert(pb.objective.elements[1].variable_index == 1);
    assert(pb.objective.elements[2].factor == 3);
    assert(pb.objective.elements[2].variable_index == 2);

    assert(pb.objective.qelements.size() == 0);

    assert(pb.objective.value == -100);

    assert(pb.vars.names.size() == 3);
    assert(pb.vars.values.size() == 3);
    assert(pb.less_constraints.size() == 3);

    assert(pb.less_constraints[0].elements.size() == 3);
    assert(pb.less_constraints[0].elements[0].factor == -1);
    assert(pb.less_constraints[0].elements[0].variable_index == 0);
    assert(pb.less_constraints[0].elements[1].factor == 1);
    assert(pb.less_constraints[0].elements[1].variable_index == 1);
    assert(pb.less_constraints[0].elements[2].factor == 1);
    assert(pb.less_constraints[0].elements[2].variable_index == 2);
    assert(pb.less_constraints[0].value == 20);

    assert(pb.less_constraints[1].elements.size() == 3);
    assert(pb.less_constraints[1].elements[0].factor == 1);
    assert(pb.less_constraints[1].elements[0].variable_index == 0);
    assert(pb.less_constraints[1].elements[1].factor == -3);
    assert(pb.less_constraints[1].elements[1].variable_index == 1);
    assert(pb.less_constraints[1].elements[2].factor == 1);
    assert(pb.less_constraints[1].elements[2].variable_index == 2);
    assert(pb.less_constraints[1].value == 30);

    assert(pb.less_constraints[2].elements.size() == 3);
    assert(pb.less_constraints[1].elements[0].factor == 1);
    assert(pb.less_constraints[2].elements[0].variable_index == 0);
    assert(pb.less_constraints[2].elements[1].factor == -3);
    assert(pb.less_constraints[2].elements[1].variable_index == 1);
    assert(pb.less_constraints[2].elements[2].factor == 1);
    assert(pb.less_constraints[2].elements[2].variable_index == 2);
    assert(pb.less_constraints[2].value == -5);

    assert(pb.vars.names[0] == "x1");
    assert(pb.vars.names[1] == "x2");
    assert(pb.vars.names[2] == "x3");

    assert(pb.vars.values[0].min == 0);
    assert(pb.vars.values[1].min == 0);
    assert(pb.vars.values[2].min == 0);

    assert(pb.vars.values[0].max == 40);
    assert(pb.vars.values[1].max == std::numeric_limits<int>::max());
    assert(pb.vars.values[2].max == std::numeric_limits<int>::max());
}

void
quadratic_5()
{
    const char* example_1 = "maximize\n"
                            "obj: x1 + 2x2 + 3x3 "
                            "+ [ -x1^2 - 17x1*x2 + 5x2^2] /2 - 100\n"
                            "st\n"
                            "time:  -x1 + x2 + x3 <= 20\n"
                            "labor:  x1 - 3x2 + x3 <= 30\n"
                            "test: x1 - 3x2 + x3 <= -5\n"
                            "bounds\n"
                            "x1 <= 40\n"
                            "end\n";

    std::istringstream iss(example_1);

    auto pb = baryonyx::make_problem(iss);

    assert(pb);
    assert(pb.type == baryonyx::objective_function_type::maximize);
    assert(pb.objective.elements.size() == 3);
    assert(pb.objective.elements[0].factor == 1);
    assert(pb.objective.elements[0].variable_index == 0);
    assert(pb.objective.elements[1].factor == 2);
    assert(pb.objective.elements[1].variable_index == 1);
    assert(pb.objective.elements[2].factor == 3);
    assert(pb.objective.elements[2].variable_index == 2);

    assert(pb.objective.qelements.size() == 3);

    assert(pb.objective.qelements[0].factor == -1.0 / 2.0);
    assert(pb.objective.qelements[0].variable_index_a == 0);
    assert(pb.objective.qelements[0].variable_index_b == 0);
    assert(pb.objective.qelements[1].factor == -17.0 / 2.0);
    assert(pb.objective.qelements[1].variable_index_a == 0);
    assert(pb.objective.qelements[1].variable_index_b == 1);
    assert(pb.objective.qelements[2].factor == 5.0 / 2.0);
    assert(pb.objective.qelements[2].variable_index_a == 1);
    assert(pb.objective.qelements[2].variable_index_b == 1);

    assert(pb.objective.value == -100);

    assert(pb.vars.names.size() == 3);
    assert(pb.vars.values.size() == 3);
    assert(pb.less_constraints.size() == 3);

    assert(pb.less_constraints[0].elements.size() == 3);
    assert(pb.less_constraints[0].elements[0].factor == -1);
    assert(pb.less_constraints[0].elements[0].variable_index == 0);
    assert(pb.less_constraints[0].elements[1].factor == 1);
    assert(pb.less_constraints[0].elements[1].variable_index == 1);
    assert(pb.less_constraints[0].elements[2].factor == 1);
    assert(pb.less_constraints[0].elements[2].variable_index == 2);
    assert(pb.less_constraints[0].value == 20);

    assert(pb.less_constraints[1].elements.size() == 3);
    assert(pb.less_constraints[1].elements[0].factor == 1);
    assert(pb.less_constraints[1].elements[0].variable_index == 0);
    assert(pb.less_constraints[1].elements[1].factor == -3);
    assert(pb.less_constraints[1].elements[1].variable_index == 1);
    assert(pb.less_constraints[1].elements[2].factor == 1);
    assert(pb.less_constraints[1].elements[2].variable_index == 2);
    assert(pb.less_constraints[1].value == 30);

    assert(pb.less_constraints[2].elements.size() == 3);
    assert(pb.less_constraints[1].elements[0].factor == 1);
    assert(pb.less_constraints[2].elements[0].variable_index == 0);
    assert(pb.less_constraints[2].elements[1].factor == -3);
    assert(pb.less_constraints[2].elements[1].variable_index == 1);
    assert(pb.less_constraints[2].elements[2].factor == 1);
    assert(pb.less_constraints[2].elements[2].variable_index == 2);
    assert(pb.less_constraints[2].value == -5);

    assert(pb.vars.names[0] == "x1");
    assert(pb.vars.names[1] == "x2");
    assert(pb.vars.names[2] == "x3");

    assert(pb.vars.values[0].min == 0);
    assert(pb.vars.values[1].min == 0);
    assert(pb.vars.values[2].min == 0);

    assert(pb.vars.values[0].max == 40);
    assert(pb.vars.values[1].max == std::numeric_limits<int>::max());
    assert(pb.vars.values[2].max == std::numeric_limits<int>::max());
}

void
quadratic_6()
{
    const char* example_1 = "maximize\n"
                            "obj: x1 + 2x2 + 3x3 "
                            "+ [ -x1*x2 - 17x1*x2 + 5x1*x2] /2 - 100\n"
                            "st\n"
                            "time:  -x1 + x2 + x3 <= 20\n"
                            "labor:  x1 - 3x2 + x3 <= 30\n"
                            "test: x1 - 3x2 + x3 <= -5\n"
                            "bounds\n"
                            "x1 <= 40\n"
                            "end\n";

    std::istringstream iss(example_1);

    auto pb = baryonyx::make_problem(iss);

    assert(pb);
    assert(pb.type == baryonyx::objective_function_type::maximize);
    assert(pb.objective.elements.size() == 3);
    assert(pb.objective.elements[0].factor == 1);
    assert(pb.objective.elements[0].variable_index == 0);
    assert(pb.objective.elements[1].factor == 2);
    assert(pb.objective.elements[1].variable_index == 1);
    assert(pb.objective.elements[2].factor == 3);
    assert(pb.objective.elements[2].variable_index == 2);

    assert(pb.objective.qelements.size() == 1);

    assert(pb.objective.qelements[0].factor == -13.0 / 2.0);
    assert(pb.objective.qelements[0].variable_index_a == 0);
    assert(pb.objective.qelements[0].variable_index_b == 1);

    assert(pb.objective.value == -100);

    assert(pb.vars.names.size() == 3);
    assert(pb.vars.values.size() == 3);
    assert(pb.less_constraints.size() == 3);

    assert(pb.less_constraints[0].elements.size() == 3);
    assert(pb.less_constraints[0].elements[0].factor == -1);
    assert(pb.less_constraints[0].elements[0].variable_index == 0);
    assert(pb.less_constraints[0].elements[1].factor == 1);
    assert(pb.less_constraints[0].elements[1].variable_index == 1);
    assert(pb.less_constraints[0].elements[2].factor == 1);
    assert(pb.less_constraints[0].elements[2].variable_index == 2);
    assert(pb.less_constraints[0].value == 20);

    assert(pb.less_constraints[1].elements.size() == 3);
    assert(pb.less_constraints[1].elements[0].factor == 1);
    assert(pb.less_constraints[1].elements[0].variable_index == 0);
    assert(pb.less_constraints[1].elements[1].factor == -3);
    assert(pb.less_constraints[1].elements[1].variable_index == 1);
    assert(pb.less_constraints[1].elements[2].factor == 1);
    assert(pb.less_constraints[1].elements[2].variable_index == 2);
    assert(pb.less_constraints[1].value == 30);

    assert(pb.less_constraints[2].elements.size() == 3);
    assert(pb.less_constraints[1].elements[0].factor == 1);
    assert(pb.less_constraints[2].elements[0].variable_index == 0);
    assert(pb.less_constraints[2].elements[1].factor == -3);
    assert(pb.less_constraints[2].elements[1].variable_index == 1);
    assert(pb.less_constraints[2].elements[2].factor == 1);
    assert(pb.less_constraints[2].elements[2].variable_index == 2);
    assert(pb.less_constraints[2].value == -5);

    assert(pb.vars.names[0] == "x1");
    assert(pb.vars.names[1] == "x2");
    assert(pb.vars.names[2] == "x3");

    assert(pb.vars.values[0].min == 0);
    assert(pb.vars.values[1].min == 0);
    assert(pb.vars.values[2].min == 0);

    assert(pb.vars.values[0].max == 40);
    assert(pb.vars.values[1].max == std::numeric_limits<int>::max());
    assert(pb.vars.values[2].max == std::numeric_limits<int>::max());
}

void
assignment_problem_0()
{
    std::ifstream ifs;

    std::vector<std::vector<int>> values(3);

    values[0] = { 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 };
    values[1] = { 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0 };
    values[2] = { 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0 };

    for (int i = 1; i != 4; ++i) {
        std::string filepath{ EXAMPLES_DIR "/assignment_problem_" };
        filepath += std::to_string(i);
        filepath += ".lp";

        auto pb = baryonyx::make_problem(filepath);

        assert(pb);
        assert(pb.status == baryonyx::file_format_error_tag::success);
        assert(pb.vars.names.size() == 16);
        assert(pb.vars.values.size() == 16);
    }
}

void
geom_30a_3_ext_1000_support()
{
    auto pb =
      baryonyx::make_problem(EXAMPLES_DIR "/geom-30a-3-ext_1000_support.lp");

    assert(pb.type == baryonyx::objective_function_type::minimize);
    assert(pb.vars.names.size() == 819);
    assert(pb.vars.values.size() == 819);

    baryonyx::index nb{ 0 };
    for (auto& elem : pb.vars.values)
        if (elem.type == baryonyx::variable_type::binary)
            ++nb;

    assert(nb == 90);
}

void
general()
{
    auto pb = baryonyx::make_problem(EXAMPLES_DIR "/general.lp");

    assert(pb.type == baryonyx::objective_function_type::minimize);
    assert(pb.vars.names.size() == 3);
    assert(pb.vars.values.size() == 3);

    baryonyx::index nb{ 0 };
    for (auto& elem : pb.vars.values)
        if (elem.type == baryonyx::variable_type::general)
            ++nb;

    assert(nb == 3);
}

void
sudoku()
{
    auto pb = baryonyx::make_problem(EXAMPLES_DIR "/sudoku.lp");

    assert(pb.vars.names.size() == 81);
    assert(pb.vars.values.size() == 81);

    for (auto& vv : pb.vars.values) {
        assert(vv.min == 1);
        assert(vv.max == 9);
        assert(vv.type == baryonyx::variable_type::general);
    }
}

void
eight_queens_puzzle()
{
    auto pb = baryonyx::make_problem(EXAMPLES_DIR "/8_queens_puzzle.lp");

    assert(pb.vars.names.size() == 64);
    assert(pb.vars.values.size() == 64);

    for (auto& vv : pb.vars.values) {
        assert(vv.min == 0);
        assert(vv.max == 1);
        assert(vv.type == baryonyx::variable_type::binary);
    }
}

void QPLIB_3852()
{
    auto pb = baryonyx::make_problem(EXAMPLES_DIR "/QPLIB_3852.lp");

    assert(pb.vars.names.size() == 231);
    assert(pb.vars.values.size() == 231);
}

void
vm()
{
    auto pb = baryonyx::make_problem(EXAMPLES_DIR "/vm.lp");
};

int
main(int ac, char* av[])
{
    small_lp();
    quadratic_lp();
    quadratic_2();
    quadratic_3();
    quadratic_4();
    quadratic_5();
    quadratic_6();
    assignment_problem_0();
    geom_30a_3_ext_1000_support();
    general();
    sudoku();
    eight_queens_puzzle();
    QPLIB_3852();
}
